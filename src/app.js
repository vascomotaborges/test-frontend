import View from "./components/view";
import actions, { APP_INIT } from "./actions";

class App {
  constructor() {
    this.view = new View();
  }
  init() {
    Object.keys(actions).map(a => this.register(a, actions[a]));
    this.fire(APP_INIT);
    this.view.render();
  }
  register(event, callback) {
    window.addEventListener(event, callback);
  }
  fire(event, detail = {}) {
    setTimeout(window.dispatchEvent(new CustomEvent(event, { detail })), 0);
  }
}

window.app = new App();
window.addEventListener("load", () => app.init());
