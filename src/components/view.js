import template from "../helpers/template";
import header from "./header";
import profile from "./profile";
import game from "./game";
import stats from "./stats";
import footer from "./footer";

class View {
  constructor() {
    this.el = document.body;
  }
  render() {
    this.el.innerHTML = template`
      ${header()}
      ${profile()}
      ${game()}
      ${stats()}
      ${footer()}
    `;
  }
}

export default View;
