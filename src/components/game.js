import template from "../helpers/template";
import update from "../helpers/update";
import board from "./game/board";
import timer from "./game/timer";
import { getState } from "../state";

import { CHANGE_PLAYER, GAME_END, MATCH_START, MATCH_END } from "../actions";

const PLAYER1_SELECTOR = "game-player-1";
const PLAYER2_SELECTOR = "game-player-2";
const SCORE1_SELECTOR = "game-player-1-score";
const SCORE2_SELECTOR = "game-player-2-score";
const MSG_SELECTOR = "game-msg";

const renderPlayer = player => {
  const num = parseInt(player.replace("player", ""), 10);
  return template`${
    num === getState("activePlayer")
      ? `<span class="game__player-${num}--selected">Player ${num}</span>`
      : `<span>Player ${num}</span>`
  }`;
};
const renderScore = player => template`${getState("score")[player]}`;

const renderMsg = (msg = "") => template`<p id="${MSG_SELECTOR}">${msg}</p>`;

const game = () => {
  app.register(CHANGE_PLAYER, () => {
    update(PLAYER1_SELECTOR, () => renderPlayer("player1"));
    update(PLAYER2_SELECTOR, () => renderPlayer("player2"));
  });
  app.register(GAME_END, () => {
    update(SCORE1_SELECTOR, () => renderScore("player1"));
    update(SCORE2_SELECTOR, () => renderScore("player2"));
  });
  app.register(MATCH_START, () => {
    update(SCORE1_SELECTOR, () => renderScore("player1"));
    update(SCORE2_SELECTOR, () => renderScore("player2"));
    update(MSG_SELECTOR, () => renderMsg(""));
  });
  app.register(MATCH_END, () => {
    update(MSG_SELECTOR, () => renderMsg("Match over!"));
  });
  return template`
    <div class="game">
      <h1 class="game__title">Tic Tac Toe</h1>
      <div class="game__board-wrapper">
        ${board(getState("matrix"))}
        <div class="game__player-1-wrapper">
          <p id="${PLAYER1_SELECTOR}" class="game__player-1">
            ${renderPlayer("player1")}
          </p>
          <p id="${SCORE1_SELECTOR}" class="game__player-1-score">
            ${renderScore("player1")}
          </span>
        </div>
        <div class="game__timer-wrapper">
          ${timer(getState("timer"))}
          ${renderMsg("")}
        </div>
        <div class="game__player-2-wrapper">
          <p id="${PLAYER2_SELECTOR}" class="game__player-2">
            ${renderPlayer("player2")}
          </p>
          <p id="${SCORE2_SELECTOR}" class="game__player-2-score">
            ${renderScore("player2")}
          </p>
        </div>
      </div>
    </div>
  `;
};

export default game;
