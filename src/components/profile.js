import template from "../helpers/template";

const profile = () => {
  const user = {
    photo:
      "https://pbs.twimg.com/profile_images/918526404584722432/-B2OJXUp_400x400.jpg",
    name: "Vasco Borges",
    age: 35,
    location: "Porto, Portugal",
    occupation: "Frontend Developer",
    about:
      "Multidisciplinary self-taught Web Developer, with 11+ years of experience in web applications development and management. Innovative perfectionist, particularly at ease with the industry's most modern tools and technologies. Ability to use creative and logical thinking to quickly identify even the most complex issues and apply swift and effective solutions."
  };
  return template`
    <div class="profile">
      <figure class="profile__photo">
        <picture>
          <img class="profile__photo-image" src="${user.photo}" alt="photo of ${user.name}" />
        </picture>
      </figure>
      <div class="profile__section">
        <p class="profile__name">${user.name}</p>
        <p class="profile__age">Age: ${user.age}</p>
        <p class="profile__location">Location: ${user.location}</p>
        <p class="profile__occupation">Occupation: ${user.occupation}</p>
      </div>
      <div class="profile__section">
        <p class="profile__about">About me <br/>${user.about}</p>
      </div>
    </div>
  `;
};

export default profile;
