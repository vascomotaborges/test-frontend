import template from "../../helpers/template";
import update from "../../helpers/update";
import calcTime from "../../helpers/calcTime";
import { getState } from "../../state";

import { GAME_START, GAME_END, TIMER_TICK } from "../../actions";

const WRAPPER_SELECTOR = "game-timer";
const TIMER_SELECTOR = "game-timer-val";

const render = () => template`
    ${
      getState("gameRunning")
        ? `<p id="${TIMER_SELECTOR}" class="game__timer-val">${calcTime(
            getState("timer")
          )}</p>`
        : `<button class="game__timer-button" onClick="app.fire('${GAME_START}')">Start Game</button>`
    }`;

const timer = () => {
  app.register(GAME_START, () => update(WRAPPER_SELECTOR, render));
  app.register(GAME_END, () => update(WRAPPER_SELECTOR, render));
  app.register(TIMER_TICK, () =>
    update(TIMER_SELECTOR, () => calcTime(getState("timer")))
  );
  return template`
    <div id="${WRAPPER_SELECTOR}" class="game__timer">
      ${render()}
    </div>
  `;
};

export default timer;
