import template from "../../helpers/template";
import update from "../../helpers/update";
import { getState } from "../../state";

import xIcon from "../icons/xIcon";
import oIcon from "../icons/oIcon";

import { GAME_START, GAME_END, CELL_CLICK } from "../../actions";

const BOARD_SELECTOR = "game-board";
const CELL_SELECTOR = "game-board-cell";

const isWinTuple = (row, col) =>
  getState("winTuples").findIndex(
    t => JSON.stringify(t) === JSON.stringify([row, col])
  ) >= 0;

const render = () => template`
  ${getState("matrix").map((r, row) => {
    return template`
      ${r.map(
        (_c, col) => template`
        <div id="${CELL_SELECTOR}-${row}-${col}" class="game__board-cell"
            onClick="app.fire('${CELL_CLICK}',{row:${row},col:${col}})">
            ${renderCell(row, col)}
        </div>
      `
      )}
    `;
  })}`;

const renderCell = (row, col) => {
  const isWin = isWinTuple(row, col);
  const val = getState("matrix")[row][col];
  return template`
  ${
    val === 1
      ? xIcon(`${isWin ? "game__board-cell-x-win" : "game__board-cell-x"} `)
      : val === 2
      ? oIcon(`${isWin ? "game__board-cell-o-win" : "game__board-cell-o"}`)
      : ""
  }`;
};

const board = () => {
  app.register(GAME_START, () => update(BOARD_SELECTOR, render));
  app.register(CELL_CLICK, ({ detail: { row, col } }) => {
    update(`${CELL_SELECTOR}-${row}-${col}`, () => renderCell(row, col));
  });
  app.register(GAME_END, () => update(BOARD_SELECTOR, render));
  return template`
    <div id="${BOARD_SELECTOR}" class="game__board">
      ${render()}
    </div>
  `;
};

export default board;
