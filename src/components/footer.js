import template from "../helpers/template";
import logoFb from "./icons/logoFb";
import logoTw from "./icons/logoTw";
import logoIn from "./icons/logoIn";
import sendIcon from "./icons/sendIcon";

const footer = () => {
  return template`
    <footer class="footer">
      <div class="footer__section">
        <h5 class="footer__section-title">About page</h5>
        <p class="footer__section-desc">Play tic tac toe anytime!</p>
        <p class="footer__section-desc">Created by @vascomotaborges</p>
      </div>
      <div class="footer__section">
        <h5 class="footer__section-title">Contacts</h5>
        <p class="footer__section-desc">(+351) 123 456 789</p>
        <p class="footer__section-desc">Rua Sá da Bandeira, 111, Porto</p>
      </div>
      <div class="footer__section">
        <h5 class="footer__section-title">Stay in touch</h5>
        <ul class="footer__social-list">
          <li class="footer__social-item">
            <a href="https://facebook.com">
              ${logoFb("footer__social-item-icon")}
            </a>
          </li>
          <li class="footer__social-item">
            <a href="https://twitter.com">
              ${logoTw("footer__social-item-icon")}
            </a>
          </li>
          <li class="footer__social-item">
            <a href="https://instagram.com">
              ${logoIn("footer__social-item-icon")}
            </a>
          </li>
        </ul>
        <form class="footer_sub-form">
          <input class="footer_sub-form-input" type="email" placeholder="Subscribe our games"/>
          <button class="footer_sub-form-btn">${sendIcon(
            "footer_sub-form-btn-icn"
          )}</button>
        </form>
      </div>
    </footer>
  `;
};

export default footer;
