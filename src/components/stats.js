import template from "../helpers/template";
import update from "../helpers/update";
import calcTime from "../helpers/calcTime";
import { getState } from "../state";

import { UPDATE_STATS } from "../actions";

const STATS_SELECTOR = "game-stats";

const renderBubbles = (wins, losses) => {
  const className = val =>
    val < 33
      ? `stats__bubble-val-danger`
      : val < 66
      ? `stats__bubble-val-warning`
      : `stats__bubble-val-success`;
  return template`
    <div class="stats__bubbles">
      <div class="stats__bubble">
        <p class="stats__bubble-val ${className(wins)}">${wins}%</p>
        <p class="stats__bubble-lbl">W</p>
      </div>
      <div class="stats__bubble">
        <p class="stats__bubble-val ${className(losses)}">${losses}%</p>
        <p class="stats__bubble-lbl">L</p>
      </div>
    </div>
  `;
};

const renderMatchesHistory = matches =>
  template`
    <ul class="stats__matches-history-list">
      ${matches.map(
        m =>
          template`<li class="stats__matches-history-item">${
            m.winner === "player2" ? "P2" : "P1"
          }</li>`
      )}
    </ul>
  `;

const render = () => {
  const matches = getState("matches");
  const [p1Wins, p2Wins, total] = matches.reduce(
    ([p1Wins, p2Wins, total], curr) => [
      p1Wins + curr.score.player1,
      p2Wins + curr.score.player2,
      total + curr.score.player1 + curr.score.player2
    ],
    [0, 0, 0]
  );
  const player1 = {
    winsPerc: Math.round((p1Wins / total) * 100) || 0,
    lossesPerc: Math.round((p2Wins / total) * 100) || 0
  };
  const player2 = {
    winsPerc: Math.round((p2Wins / total) * 100) || 0,
    lossesPerc: Math.round((p1Wins / total) * 100) || 0
  };
  return template`
    <h2 class="stats__title">Awesome statistics</h2>
    <h3 class="stats__subtitle">All statistics in one place!</h3>
    <div class="stats__victories-wrapper">
      <p class="stats__victories-title">Game victories %</p>
      <div class="stats__victories-player1">
        <p class="stats__player-title">Player 1</p>
        ${renderBubbles(player1.winsPerc, player1.lossesPerc)}
      </div>
      <div class="stats__victories-player2">
        <p class="stats__player-title">Player 2</p>
        ${renderBubbles(player2.winsPerc, player2.lossesPerc)}
    </div>
    </div>
      ${
        matches.length > 0
          ? template`
              <div class="stats__matches-history">
                <p class="stats__matches-history-title">Matches History</p>
                ${renderMatchesHistory(matches)}
              </div>`
          : ``
      }
      <div class="stats__total-time">
        <p class="stats__total-time-title">Total Time</p>
        <p class="stats__total-time-val">${calcTime(getState("totalTime"))}</p>
      </div>
  `;
};

const stats = () => {
  app.register(UPDATE_STATS, () => update(STATS_SELECTOR, render));
  return template`
    <div id="${STATS_SELECTOR}" class="stats">
      ${render()}
    </div>
  `;
};

export default stats;
