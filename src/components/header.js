import template from "../helpers/template";
import logo from "./icons/logo";

const header = () => {
  const app = {
    title: "Frontend Developer",
    desc: "Challenge"
  };
  return template`
    <header class="header">
      <div class="header__logo">
        ${logo("header__logo-icon")}
      </div>
      <div class="header__app">
        <p class="header__app-title">${app.title}</p>
        <p class="header__app-desc">${app.desc}</p>
      </div>
    </header>
  `;
};

export default header;
