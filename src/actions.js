import { getState, setState, loadState, resetState } from "./state";
import { win, full } from "./helpers/math";

export const APP_INIT = "appInit";
export const TIMER_TICK = "timerTick";
export const CELL_CLICK = "cellClick";
export const GAME_START = "gameStart";
export const GAME_END = "gameEnd";
export const GAME_WIN = "gameWin";
export const CHANGE_PLAYER = "changePlayer";
export const MATCH_START = "matchStart";
export const MATCH_END = "matchEnd";
export const UPDATE_STATS = "updateStats";

let interval;
const actions = {
  [APP_INIT]: _ev => {
    loadState();
  },

  [CELL_CLICK]: ({ detail: { row, col } }) => {
    const matrix = getState("matrix");
    const player = getState("activePlayer");
    if (getState("gameRunning") && matrix[row][col] === 0) {
      const newMatrix = matrix.map((x, i) =>
        i === row ? x.map((y, j) => (j === col ? player : y)) : x
      );
      setState("matrix", newMatrix);
      const winTuples = win(newMatrix);
      if (winTuples) {
        app.fire(GAME_WIN, { winTuples });
      } else if (full(newMatrix)) {
        app.fire(GAME_END);
      } else {
        app.fire(CHANGE_PLAYER);
      }
    }
  },

  [CHANGE_PLAYER]: _ev => {
    setState("activePlayer", getState("activePlayer") === 1 ? 2 : 1);
  },

  [GAME_START]: _ev => {
    if (getState("score").player1 == 5 || getState("score").player2 == 5) {
      app.fire(MATCH_START);
    }
    app.fire(CHANGE_PLAYER);
    resetState("matrix");
    resetState("winTuples");
    setState("gameRunning", true);
    interval = setInterval(() => {
      setState("timer", getState("timer") + 1);
      app.fire(TIMER_TICK);
    }, 1000);
  },

  [GAME_WIN]: ({ detail: { winTuples } }) => {
    const key = `player${getState("activePlayer")}`;
    const newScore = {
      ...getState("score"),
      [key]: getState("score")[key] + 1
    };
    setState("winTuples", winTuples);
    setState("score", newScore);
    app.fire(GAME_END);
    if (getState("score")[key] === 5) {
      app.fire(MATCH_END, { winner: key });
    }
  },

  [GAME_END]: _ev => {
    setState("gameRunning", false);
    setState("totalTime", getState("totalTime") + getState("timer"));
    clearInterval(interval);
    resetState("timer");
  },

  [MATCH_START]: _ev => {
    resetState("score");
  },

  [MATCH_END]: ({ detail: { winner } }) => {
    setState("matches", [
      ...getState("matches"),
      { winner, score: getState("score") }
    ]);
    app.fire(UPDATE_STATS);
  }
};

export default actions;
