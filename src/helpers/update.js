const update = (selector, renderFunc) => {
  const el = document.querySelector(`#${selector}`);
  const html = renderFunc();
  if (el && html) {
    el.innerHTML = html;
  }
};

export default update;
