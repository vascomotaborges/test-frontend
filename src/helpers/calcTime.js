const calcTime = time => {
  const hrs = ~~(time / 3600);
  const mins = ~~((time % 3600) / 60);
  const secs = ~~time % 60;
  let str = "";

  if (hrs > 0) {
    str += "" + hrs + ":" + (mins < 10 ? "0" : "");
  }

  str += "" + mins + ":" + (secs < 10 ? "0" : "");
  str += "" + secs;
  return str;
};

export default calcTime;
