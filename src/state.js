const DEFAULT_SIZE = 3;
const initialState = {
  gameRunning: false,
  timer: 0,
  size: DEFAULT_SIZE,
  matrix: new Array(DEFAULT_SIZE).fill(new Array(DEFAULT_SIZE).fill(0)),
  activePlayer: 0,
  score: {
    player1: 0,
    player2: 0
  },
  winTuples: [],
  matches: [],
  totalTime: 0
};

const getState = key => {
  return app.state[key];
};

const setState = (key, val) => {
  app.state = { ...app.state, [key]: val };
};

const loadState = () => {
  app.state = initialState;
};

const resetState = key => {
  if (key) {
    setState(key, initialState[key]);
  } else {
    loadState();
  }
};

export { getState, setState, loadState, resetState };
